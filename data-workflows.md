# Data Workflows

This document describes possible strategies to handle data workflows
within the UA10 platform.

The aim of data workflows is to allow researchers and editors to manage
data-related content through automated scripts, in order to retain
integrity of data and visualizations, to allow to manage metadata
associated to content in a consistent way, to publish/republish
data-related content as needed, and to manage access rights (editing
content, publishing rendered data or visualizations, exporting or
publishing raw or processed data selectively)

Data-related content mainly consists of:

* **raw data**: source tabular (CSV, etc.) data, GIS shapefiles and project files
* **processed data**: tabular data or GIS data obtained by processing raw data through scripts (e.g. the JSON dataset (~1MiB) used for the European MetroMonitor web data visualizations, produced by processing the Oxford Economics European Cities raw dataset (~80MiB) through R scripts)
* **'rendered' data**: charts and maps as individual files (EPS, AI, JPEG, PNG, etc.) as exported from past publications
* **web-native data visualizations** (JavaScript-based)

## Development

We currently have a complete set of *rendered data* from all the past
Urban Age publications, with embedded metadata tags including:

* description
* themes
* topics
* cities
* countries
* publication (if applicable)
* UA conference (if applicable)
* year of publication (if applicable)
* source data year (if applicable/available)
* item type (e.g. data:modalsplit for modal split charts)

We haven't undertaken a full review of source (raw/processed) data yet:
in fact one of the purposes of the data workflows development work is to
highlight which source datasets need to be linked to each item of
rendered data, so that we can then trace the source data (or, in simple
cases such as modal split charts, reverse-engineer it from the rendered
data items).

The suggested strategy involves augmenting the data and related
processing scripts in stages, in order to always have a usable 'MVP'.

The following steps can be applied to individual data types, e.g.
starting with 'simpler' cases such as modal splits, and progressively
reiterated for other data types (e.g. transport maps).

GIS-based data types will likely require very specific treatment, and
possibly some preliminary cleanup work including exporting cleaned-up
shapefiles to a format that can be easily processed on a GNU/Linux
virtual machine in order to fully automate the process on cloud servers
without having to manually trigger procedures on a Windows workstation
in ArcGIS.

### Stage 1 - metadata (rendered data)

#### Aim

To allow data managers to produce a full metadata dataset from the
folders of rendered data.

#### Output

Scripts to process folders of rendered data, exporting:

* a JSON dump of the metadata, to be further processed (e.g. to group
  rendered data items according to themes, city, source data year, etc.)
* JPEG thumbnails of each item (if available as embedded metadata)

### Stage 2 - visualizing metadata (rendered data)

#### Aim

To allow editors to browse/filter rendered data items

#### Output

New filters in test search app using the JSON dump of metadata and
thumbnails extracted via the Stage 1 tools.

### Stage 3 - describing relationships between items

#### Aim

To produce formal descriptions of relationships between content items
we want to use throughout the UA10 platform. These:

* may be content type-specific
* may be one or more per content type
* may be used in different contexts (e.g. within the CMS backend,
  within a 'narrative', within a *small multiple* data visualization,
  etc.)

For example, we may want to establish a relationship

* between *modal split charts*
* for the *same city*
* based on *data from different years*

e.g. *All modal split charts for London, based on any source data
years available*.

or

* betweem *modal split charts*
* across *cities from a set*
* published *in years within a set*

e.g. *All modal split charts for London, São Paulo, Delhi and Bogotá,
published in UA conference newspapers in 2008, 2012, 2014*.
  
#### Output

Formal descriptions of relationships between content items, in a
machine-processable format (e.g. JSON or R or Javascript code), with
embedded documentation.

### Stage 4 - Producing proxy (dummy) data structures for source data

At this stage, we have a good sense of how we may want to search/filter
data content items and we know how we want to visualize data (starting
from a 1:1 mapping of the rendered data items to web native
visualizations. We won't have code to generate web native data
visualizations yet, and we may not have located nor polished yet any
source data needed to create these visualizations.

Based on the rendered data available, however, we should now be able to
compile formal descriptions of the source data to be used by the code
that will implement each type of visualization or chart, and we can
generate dummy data, if real source data is not available or hasn't
been located yet.

We can now (possibly in parallel) expand the workflow backwards (to
raw source data) and forward (to web native visualizations).

### Stage 5a - From raw data to tidy data

#### Aim

To produce scripts to tidy up raw data for each type of visualization/chart
and export it in the format identified in Stage 4.

This involves locating raw source data and writing all the code needed
to obtain the processed source data for the data visualization code
(Stage 5b).

### Stage 5b - Web native data visualizations

#### Aim

To write code for each type of data visualization/chart.

This can be based on dummy data (Stage 4) or real data (Stage 5a).

### Stage 6 - Integration of data visualizations

According to the relationships between items of content described in
Stage 3, we may need to combine visualizations/charts.

### Stage 7 - Integration of rendered data items

Depending on priority/resources/time constraints, we will likely need
to feature parts of content as rendered data rather than web native
visualizations: we will likely still need to integrate both types
of visualizations (rendered/native) within a coherent interface.
This may involve adjusting the vector sources of rendered data and
re-exporting items (e.g. with consistent padding, scale, etc.),
as well as writing the code needed to fetch and display content as
available and as appropriate (e.g. rendered chart if no native version
of a given chart type is available).


